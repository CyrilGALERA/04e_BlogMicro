# version 02 = 01 nettoyée en fonct-route et sous fonct -secret et -post-url

from datetime import date, datetime
from fileinput import filename
from hashlib import sha512
import json
from pickle import GET
from flask import Flask, render_template , request, redirect

app = Flask(__name__,
            static_url_path='/DATA', 
            static_folder='DATA')

# @app.route("/")
# def hello_world():
#     return "<p>Hello, World!</p>"

@app.route('/')
def redirection():
  return redirect('blog')


@app.route('/blog/')
def blog():
  return render_template('vue.html')


@app.route('/blog/', methods=['POST'])
def publier_post_url():
  if check_secret() :
    # print('secret identique !')
    url = create_json_post_date_url()
    return url
  else:
    # print('secret invalide !')
    return redirect ("http://127.0.0.1:5000/", code=403)


def check_secret():
  # Recuperer le Secret du Formulaire
  secretForm = request.form['Secret']
  secretForm = secretForm.encode()
  secretFormHash = sha512(secretForm).hexdigest()
  # Recuperer le Secret du Fichier User
  with open('pwdUSER.txt') as pwdFile :
    secretFile = pwdFile.readline()
  # Comparer 
  if secretFormHash == secretFile :
    print('secret identique !')
    return True
  else:
    # Secret invalide
    return False

def create_json_post_date_url():
  print('create_json_post_date_url')

  # Obtenir/créer la date et la formater pour le fich.json et le contenu date
  date = datetime.now()
  date_file = date.strftime('%Y%m%d_%H%M%S')
  date_time = date.strftime('%Y-%m-%d_%H:%M:%S')

  # Créer le urlNAME
  urlNAME = 'http://127.0.0.1:5000/DATA/'+ date_file +'.json'
  # Créer le fileNAME = dateName
  date_filePath = './DATA/'+ date_file +'.json'
  # Obtenir form[contenu]
  contenu = request.form['Article']

  # Verif des data à sauvegarder:
  print("date url date.json = ",date_file," ", date_time," ",urlNAME, " ", date_filePath)

  # Créer le fichier postDate.json
  with open(date_filePath, 'w') as outfile:
      article = {
          "url": urlNAME,
          "contenu": contenu, 
          "date": date_time
      }
      json.dump(article, outfile)

  # Ouvrir/Lire le fich posts.json
  with open('./DATA/posts.json','r') as postsFile:
    # json doit etre un tab [] si tu veux utiliser ensuite .append(url)
    postsDataDict = json.load(postsFile)
    print("postsDataDict = ",postsDataDict)
    postsUrlTab = postsDataDict['post']
    print("postsUrlTab = ",postsUrlTab)
    # Ajouter le nouveau post créée urlNAME
    postsUrlTab.append(urlNAME)
    print("postsUrlTab = ",postsUrlTab)
    print("postsDataDict = ",postsDataDict)

  # Ouvrir/Ecrire les modif
  with open('./DATA/posts.json','w') as postsFile:
      json.dump(postsDataDict, postsFile, indent=4)
  return urlNAME