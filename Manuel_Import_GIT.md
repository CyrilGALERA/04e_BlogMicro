# Import du projet GIT sur PC
( Utilisation: pour publier un post, le password est :"azerty")
.  

## 1.1) Créer un projet python vierge
- mkdir BlogMicro
- cd BlogMicro
- python3 -m venv venv

## 1.2) Activer l env de python
. venv/bin/activate  
s'affiche:  
(venv) ...   
.   
   
## 2.1) Installer Flask  
(venv) pip install Flask   
.   
    
## 2.2) Verifier  
(venv) flask --version  
S'affiche:  
- Python 3.8.10
- Flask 2.1.1
- Werkzeug 2.1.1

## 3.1) Importer le projet git  
git clone https://gitlab.com/CyrilGALERA/04e_BlogMicro.git   
.  
    
## 3.2) Déplacer les fichier du clone dans le rep venv
- cd 04e_BlogMicro
- cp -rf * ../venv/.

## 4) Exécuter le programme app.py
- export FLASK_APP=app (pas la peine, c est automatique si le prog = app.py)
- flask run  
  
S'affiche:  
- ...
- Running on http://127.0.0.1:5000/
- => Ctrl Click sur le lien http