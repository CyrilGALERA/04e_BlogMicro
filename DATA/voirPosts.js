// version 02: avec le trie ante-chronologique

// MODEL = DATA =================================================================
function getPostsJson() {
  fetch('../DATA/posts.json')
    // Sortir le fichier posts.json en une var (json_data)
    .then( reponse => reponse.json() )
    // la nouvelle var (json_data) = reponse.json()
    .then( json_data => {
        console.log("POSTS JSON = ",json_data);

        // get All articles.json from posts.json
        let tabArticles = [];
        let nb_print = 0;
        json_data.post.forEach( function(url) {
        fetch(url)
            .then( rep => rep.json() )
            .then( json_article => {
                tabArticles.push( json_article );
                console.log("tabArticles = ",tabArticles);
                nb_print ++;
                console.log("nb_print = ", nb_print)
                printList( tabArticles );
            })
            .catch(error => console.log(error));
        })
    })
    .catch(error => console.log(error));
}

// CONTROL (+VIEW?) ==============================================================

function printList ( tabArticles ) {
    
    // 1) Trier la list
    tabArticles.sort( (articleA, articleB) => {
        // Ne fonctionne pas : affiche NaN Not a Number
        // console.log("A - B = ", articleA.date - articleB.date )
        // return (articleA.date - articleB.date) 

        // Ne fonctionne pas : affiche true or false
        // console.log("A < B = ", articleA.date < articleB.date )
        // return (articleA.date < articleB.date) 

        // Fonctionne bien! affiche -1 ou +1
        // console.log("A < B ? +1 -1 ", (articleA.date < articleB.date) ? 1 : -1 )
        return (articleA.date < articleB.date) ? 1 : -1;
    });
    console.log(' printList ', tabArticles);

    // 2) Vider la div HTML list
    let divListArticles = document.getElementById('listArticles');
    divListArticles.innerHTML = '';

    // 3) Afficher
    tabArticles.forEach( article => {display_ARTICLE(article)} );
}

// VIEW ======================================================================
function display_ARTICLE( article_data ){
  let divListArticles = document.getElementById('listArticles');
  let divArticle = document.createElement('div');
  divListArticles.appendChild(divArticle);

  let divDateFile = document.createElement('div');
  divDateFile.setAttribute('id', `divDateFile`);
  divArticle.appendChild(divDateFile);

  let h3Date = document.createElement('h3');
  h3Date.innerText = article_data.date;
  divDateFile.appendChild(h3Date);

  // let divURL = document.createElement('div');
  let linkA = document.createElement('a');
  linkA.setAttribute('href', article_data.url);
  // Re Format nice filename
  // let dateStr =  article_data.date.split('-');
  // let filename = dateStr[0]+dateStr[1];
  // dateStr = dateStr[2].split(' ');
  // filename += dateStr[0];
  // dateStr = dateStr[1].split(':');
  // filename += '_'+dateStr[0]+dateStr[1]+dateStr[2];
  // let createAText = document.createTextNode(filename+'.json');
  //
  let linkA_Text = document.createTextNode(article_data.date+'.json');
  //
  linkA.appendChild(linkA_Text);
  divDateFile.appendChild(linkA);

  let divContenu = document.createElement('div');
  divContenu.setAttribute('id', `divContenu`);

  let pContenu = document.createElement('p');
  // Si format texte normal:
  // pContenu.innerText = article_data.contenu;

  // Si écrit en format Markdown :
  // Ajouter:
  // <script src="https://cdnjs.cloudflare.com/ajax/libs/markdown-it/....
  var md = window.markdownit();
  var md2html = md.render(article_data.contenu);
  pContenu.innerHTML += md2html;

  divContenu.appendChild(pContenu);
  divArticle.appendChild(divContenu);
}

// MAIN =======================================================================
getPostsJson();