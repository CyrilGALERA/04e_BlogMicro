// version01: sans le trie des fetch

// MODEL = DATA =================================================================
function getPostsJson() {
  fetch('../DATA/posts.json')
      // Sortir le fichier json en une var (posts_data)
      .then( 
          reponse => reponse.json() 
      )
      // la nouvelle var (json_data) = reponse.json()
      .then(
          json_data => {
              console.log("POSTS JSON = ",json_data);

              json_data.post.forEach( function(url) {
                  console.log("POST i = ",url);
                  getDateArticleJson(url) 
              });
          }
      )
      .catch(error => console.log(error));
}

function getDateArticleJson(url) {
fetch(url)
  // Sortir le fichier json 
  .then(
      rep => rep.json()
  )
  //
  .then(
      json_data => {
          console.log("Articles JSON = ",json_data);
          display_ARTICLE( json_data );
      }
  )
  .catch(e => console.log(e));
}

// VIEW ======================================================================
function display_ARTICLE( article_data ){

  console.log("ARTICLE = ", article_data);
  console.log("article_data[date] = ", article_data.date );

  let divListArticles = document.getElementById('listArticles');
  let divArticle = document.createElement('div');
  divListArticles.appendChild(divArticle);

  let divDateFile = document.createElement('div');
  divDateFile.setAttribute('id', `divDateFile`);
  divArticle.appendChild(divDateFile);

  let h3Date = document.createElement('h3');
  h3Date.innerText = article_data.date;
  divDateFile.appendChild(h3Date);

  // let divURL = document.createElement('div');
  let linkA = document.createElement('a');
  linkA.setAttribute('href', article_data.url);
  // Re Format nice filename
  // let dateStr =  article_data.date.split('-');
  // let filename = dateStr[0]+dateStr[1];
  // dateStr = dateStr[2].split(' ');
  // filename += dateStr[0];
  // dateStr = dateStr[1].split(':');
  // filename += '_'+dateStr[0]+dateStr[1]+dateStr[2];
  // let createAText = document.createTextNode(filename+'.json');
  //
  let linkA_Text = document.createTextNode(article_data.date+'.json');
  //
  linkA.appendChild(linkA_Text);
  divDateFile.appendChild(linkA);

  let divContenu = document.createElement('div');
  divContenu.setAttribute('id', `divContenu`);

  let pContenu = document.createElement('p');
  // Si format texte normal:
  // pContenu.innerText = article_data.contenu;

  // Si écrit en format Markdown :
  // Ajouter:
  // <script src="https://cdnjs.cloudflare.com/ajax/libs/markdown-it/....
  var md = window.markdownit();
  var md2html = md.render(article_data.contenu);
  pContenu.innerHTML += md2html;

  divContenu.appendChild(pContenu);
  divArticle.appendChild(divContenu);
}

// MAIN =======================================================================
getPostsJson();